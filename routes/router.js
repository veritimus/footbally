const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const moment = require('moment');
const User = require('../models/User');
const Club = require('../models/Club');
const { Player } = require('../models/Player');
const PlayerGenerator = require('../app/player-generator');
const { check, validationResult } = require('express-validator/check');
const { matchedData } = require('express-validator/filter');
const bcrypt = require('bcrypt');
const alerts = require('../app/alerts');

router.get('/login', (req, res, next) => {
   res.render('login');
});

router.post('/login', [
   check('email', alerts.info.emailTip)
      .exists()
      .withMessage(alerts.info.requiredField)
      .isEmail(),
   check('password', alerts.info.passwordTip)
      .exists()
      .withMessage(alerts.info.requiredField)
      .isLength({ min: 8, max: 25 })
      .isAlphanumeric(),
], (req, res, next) => {
   const errors = validationResult(req);

   if(!errors.isEmpty()) {
      return res.render('login', { errors: errors.mapped() });
   } else {
      const data = matchedData(req);

      User.authenticate({ email: data.email, password: data.password }, (err, user) => {
         if(err)
            return res.render('login', { invalidCredentials: err });
         
         req.session.userID = user._id;
         res.redirect('/');
      });
   }
});

router.get('/logout', (req, res, next) => {
   req.session.destroy(err => {
      if(err)
         return next(err);

      res.redirect('/');
   });
});

router.get('/register', (req, res, next) => {
   res.render('register', {
      tips: {
         emailTip: alerts.info.emailTip,
         usernameTip: alerts.info.usernameTip,
         clubNameTip: alerts.info.clubNameTip,
         passwordTip: alerts.info.passwordTip
      }
   });
});

router.post('/register', [
   check('email', alerts.info.emailTip)
      .exists()
      .withMessage(alerts.info.requiredField)
      .isEmail(),
   check('username', alerts.info.usernameTip)
      .exists()
      .withMessage(alerts.info.requiredField)
      .isLength({ min: 3, max: 20 })
      .isAlphanumeric(),
   check('clubName', alerts.info.clubNameTip)
      .exists()
      .withMessage(alerts.info.requiredField)
      .isLength({ min: 3, max: 20 })
      .matches(/^[0-9a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ ]{3,20}$/),
   check('password', alerts.info.passwordTip)
      .exists()
      .withMessage(alerts.info.requiredField)
      .isLength({ min: 8, max: 25 })
      .isAlphanumeric(),
   check('passwordConfirm', alerts.info.equalsPasswords)
      .exists()
      .withMessage(alerts.info.requiredField)
      .custom((value, { req }) => value === req.body.password)
], (req, res, next) => {
   const errors = validationResult(req);

   if(!errors.isEmpty()) {
      return res.render('register', { errors: errors.mapped() });
   } else {
      const data = matchedData(req);
      const error = {};

      User.findOne({ $or: [{ email: data.email }, { username: data.username }] }, (err, user) => {
         if(user) {
            if(user.email === data.email) {
               error.unavailableEmail = alerts.errors.unavailableEmail;
            }

            if(user.username === data.username) {
               error.unavailableUsername = alerts.errors.unavailableUsername;
            }
         }

         Club.findOne({ clubName: data.clubName }, (err, club) => {
            if(club)
               error.unavailableClubName = alerts.errors.unavailableClubName;

            if(Object.keys(error).length > 0)
               return res.render('register', { error });

            User.register(data, (err, registeredUser) => {
               if(err)
                  return next(err);

               res.redirect('/');
            });
         });
      });
   }
});

router.get('/profile', (req, res, next) => {
   User.findById(req.session.userID, (err, user) => {
      if(err)
         return next(err);

      const { email, username, dateOfRegistration } = user;

      res.render('profile', {
         email,
         username,
         dateOfRegistration: moment(dateOfRegistration).format('DD.MM.YYYY')
      });
   });
});

router.get('/profile/edit', (req, res, next) => {
   res.render('profile-edit');
});

router.post('/profile/edit', [
   check('email', alerts.info.emailTip)
      .optional({ checkFalsy: true })
      .isEmail(),
   check('password', alerts.info.passwordTip)
      .optional({ checkFalsy: true })
      .isLength({ min: 8, max: 25 })
      .isAlphanumeric(),
   check('passwordConfirm', alerts.info.equalsPasswords)
      .optional({ checkFalsy: true })
      .custom((value, { req }) => value === req.body.password)
], (req, res, next) => {
   const errors = validationResult(req);
   
   if(!errors.isEmpty()) {
      return res.render('profile-edit', { errors: errors.mapped() });
   } else {
      const data = matchedData(req);

      if(data.email) {
         User.findOne({ email: data.email }, (err, user) => {
            if(err)
               return next(err);

            if(user) {
               const unavailableEmail = alerts.errors.unavailableEmail;

               return res.render('profile-edit', { unavailableEmail });
            } else {
               User.findById(req.session.userID, (err, sessionUser) => {
                  if(err)
                     return next(err);

                  if(data.password) {
                     sessionUser.password = bcrypt.hashSync(data.password, 8);
                  }
                  
                  sessionUser.email = data.email;
                  
                  sessionUser.save(err => {
                     if(err)
                        return next(err);

                     const dataChangeSuccess = alerts.success.dataChangeSuccess;

                     return res.render('profile-edit', { dataChangeSuccess });
                  });
               });
            }
         });
      } else {
         User.findById(req.session.userID, (err, sessionUser) => {
            if(err)
               return next(err);

            sessionUser.password = bcrypt.hashSync(data.password, 8);
            
            sessionUser.save(err => {
               if(err)
                  return next(err);

               const dataChangeSuccess = alerts.success.dataChangeSuccess;

               return res.render('profile-edit', { dataChangeSuccess });
            });
         });
      }
   }
});

router.get('/profile/remove', (req, res, next) => {
   Club.findOneAndRemove({ owner: req.session.userID }, (err, club) => {
      if(err)
         return next(err);

      User.findByIdAndRemove(req.session.userID, (err, club) => {
         if(err)
            return next(err);

         req.session.destroy(err => {
            if(err)
               return next(err);

            res.redirect('/');
         });
      });
   });
});

router.get('/', (req, res, next) => {
   Club.findOne({ owner: req.session.userID }, (err, club) => {
      if(err)
         return next(err);

      const { clubName, arenaName, budget, teamPlayers } = club;
      const owner = true;
      
      User.findById(club.owner, (err, user) => {
         if(err)
            return next(err);

         const { username, dateOfRegistration } = user;

         return res.render('club', {
            username,
            owner,
            clubName,
            arenaName,
            budget,
            teamPlayers: teamPlayers.length,
            dateOfRegistration: moment(dateOfRegistration).format('DD.MM.YYYY')
         });
      });
   });
});

router.get('/club/edit', (req, res, next) => {
   Club.findOne({ owner: req.session.userID }, (err, club) => {
      if(err)
         return next(err);

      const { clubName, arenaName } = club;

      return res.render('club-edit', {
         clubName,
         arenaName
      });
   });
});

router.post('/club/edit', [
   check('clubName', alerts.info.clubNameTip)
      .exists()
      .isLength({ min: 3, max: 20 })
      .matches(/^[0-9a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ ]{3,20}$/),
   check('arenaName', alerts.info.arenaNameTip)
      .exists()
      .isLength({ min: 5, max: 27 })
      .matches(/^[0-9a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ' ]{5,27}$/)
], (req, res, next) => {
   const errors = validationResult(req);

   if(!errors.isEmpty()) {
      Club.findOne({ owner: req.session.userID }, (err, club) => {
         if(err)
            return next(err);

         const { clubName, arenaName } = club;

         return res.render('club-edit', {
            clubName,
            arenaName,
            errors: errors.mapped()
         });
      });
   } else {
      const data = matchedData(req);

      Club.findOne({ owner: req.session.userID }, (err, club) => {
         if(err)
            return next(err);

         club.clubName = data.clubName;
         club.arenaName = data.arenaName;

         club.save(err => {
            if(err)
               return next(err);

            res.redirect('/');
         });
      });
   }
});

router.get('/club/:id', (req, res, next) => {
   const { id } = req.params;

   Club.findById(id, (err, club) => {
      if(err)
         return next(err);

      if(!club)
         return res.status(404).render('404');

      const { clubName, arenaName, budget, teamPlayers } = club;
      const owner = (club.owner == req.session.userID) ? true : false;
      
      User.findById(club.owner, (err, user) => {
         if(err)
            return next(err);

         const { username, dateOfRegistration } = user;

         return res.render('club', {
            username,
            owner,
            clubName,
            arenaName,
            budget,
            teamPlayers: teamPlayers.length,
            dateOfRegistration: moment(dateOfRegistration).format('DD.MM.YYYY')
         });
      });
   });
});

router.get('/player/:id', (req, res, next) => {
   const { id } = req.params;

   Club.findOne({ 'teamPlayers._id': id }, (err, club) => {
      if(err)
         return next(err);

      if(!club)
         return res.status(404).render('404');

      const player = club.teamPlayers.id(id);
      const { firstName, lastName, position, nationality, age, skillsRate, talentRate, preferedLeg, salary, skills, speech } = player;
      const skillsPlainObject = skills.toObject();
      const owner = (club.owner == req.session.userID) ? true : false;

      return res.render('player', {
         id,
         owner,
         clubName: club.clubName,
         firstName,
         lastName,
         position,
         nationality,
         age,
         skillsRate,
         talentRate,
         preferedLeg,
         salary,
         skills: skillsPlainObject,
         speech
      });
   });
});

router.get('/player/:id/edit', (req, res, next) => {
   const { id } = req.params;
   
   Club.findOne({ 'teamPlayers._id': id }, (err, club) => {
      if(err)
         return next(err);

      if(!club)
         return res.status(404).render('404');

      const player = club.teamPlayers.id(id);
      const { firstName, lastName, speech } = player;
      const owner = (club.owner == req.session.userID) ? true : false;

      if(owner) {
         return res.render('player-edit', {
            id,
            firstName,
            lastName,
            speech
         });
      }

      res.redirect('/403');
   });
});

router.post('/player/:id/edit', [
   check('speech', alerts.info.speechTip)
      .exists()
      .withMessage(alerts.info.requiredField)
      .isLength({ min: 1, max: 150 })
      .matches(/^[0-9a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ,.?!-: ]{1,150}$/)
], (req, res, next) => {
   const { id } = req.params;
   const errors = validationResult(req);

   if(!errors.isEmpty()) {
      Club.findOne({ 'teamPlayers._id': id }, (err, club) => {
         if(err)
            return next(err);
   
         const player = club.teamPlayers.id(id);
         const { firstName, lastName, speech } = player;
         const owner = (club.owner == req.session.userID) ? true : false;
   
         return res.render('player-edit', {
            id,
            firstName,
            lastName,
            speech,
            errors: errors.mapped()
         });
      });
   } else {
      const data = matchedData(req);

      Club.findOne({ 'teamPlayers._id': id }, (err, club) => {
         if(err)
            return next(err);

         const player = club.teamPlayers.id(id);
         player.speech = data.speech;

         club.save((err, updatedClub) => {
            if(err)
               return next(err);

            res.redirect(`/player/${updatedClub.teamPlayers.id(id)._id}`);
         });
      });
   }
});

router.get('/player/:id/remove', (req, res, next) => {
   const { id } = req.params;

   Club.findOne({ 'teamPlayers._id': id }, (err, club) => {
      if(err)
         return next(err);

      if(!club)
         return res.status(404).render('404');

      const owner = (club.owner == req.session.userID) ? true : false;

      if(owner) {
         club.teamPlayers.id(id).remove();

         club.save(err => {
            if(err)
               return next(err);

            return res.redirect(`/team`);
         });
      } else {
         res.redirect('/403');
      }
   });
});

router.get('/team', (req, res, next) => {
   Club.findOne({ owner: req.session.userID }, (err, club) => {
      if(err)
         return next(err);

      if(club.teamPlayers.length === 0)
         return res.render('team');
      else {
         const players = club.teamPlayers.toObject();
         res.render('team', { players });
      }
   });
});

router.get('/team/newPlayer', (req, res, next) => {
   res.render('player-new');
});

router.post('/team/newPlayer/generate', (req, res, next) => {
   const player = new PlayerGenerator(req.body);

   res.render('player-new', {
      player
   });
});

router.post('/team/newPlayer/save', (req, res, next) => {
   const { firstName, lastName, nationality, years, months, days, preferedLeg, position, skillsRate, talentRate, salary } = req.body;

   let playerModel = {
      firstName,
      lastName,
      position,
      nationality,
      age: {
         years: +years,
         months: +months,
         days: +days
      },
      skillsRate: +skillsRate,
      talentRate: +talentRate,
      preferedLeg,
      salary: +salary
   };

   if(position === 'GK') {
      playerModel.skills = {
         RE: {
            current: +req.body.currentRE,
            potential: +req.body.potentialRE
         },
         GP: {
            current: +req.body.currentGP,
            potential: +req.body.potentialGP
         },
         IN: {
            current: +req.body.currentIN,
            potential: +req.body.potentialIN
         },
         CT: {
            current: +req.body.currentCT,
            potential: +req.body.potentialCT
         },
         OR: {
            current: +req.body.currentOR,
            potential: +req.body.potentialOR
         }
      }
   } else {
      playerModel.skills = {
         SC: {
            current: +req.body.currentSC,
            potential: +req.body.potentialSC
         },
         OP: {
            current: +req.body.currentOP,
            potential: +req.body.potentialOP
         },
         BC: {
            current: +req.body.currentBC,
            potential: +req.body.potentialBC
         },
         PA: {
            current: +req.body.currentPA,
            potential: +req.body.potentialPA
         },
         AE: {
            current: +req.body.currentAE,
            potential: +req.body.potentialAE
         },
         CO: {
            current: +req.body.currentCO,
            potential: +req.body.potentialCO
         },
         TA: {
            current: +req.body.currentTA,
            potential: +req.body.potentialTA
         },
         DP: {
            current: +req.body.currentDP,
            potential: +req.body.potentialDP
         }
      }
   }

   User.findById(req.session.userID, (err, user) => {
      if(err)
         return next(err);

      Club.findOne({ owner: user._id }, (err, club) => {
         if(err)
            return next(err);

         const player = club.teamPlayers.create(playerModel);
         club.teamPlayers.push(player);

         club.save(err => {
            if(err) 
               return next(err);

            res.redirect(`/player/${player._id}`);
         });
      });
   });
});

router.get('/403', (req, res, next) => {
   res.status(403).render('403');
})

router.get('*', (req, res, next) => {
   res.status(404).render('404');
})

module.exports = router;