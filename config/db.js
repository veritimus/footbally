const mongoose = require('mongoose');
const keys = require('./keys');
const autoIncrement = require('mongoose-auto-increment');

mongoose.Promise = global.Promise;

const connection = mongoose.createConnection(keys.mongoURI);
const Schema = mongoose.Schema;

autoIncrement.initialize(connection);

module.exports = {
   connection,
   Schema,
   autoIncrement
}