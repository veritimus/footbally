module.exports = {
   errors: {
      unavailableEmail: 'Podany email jest już zajęty',
      unavailableUsername: 'Podana nazwa użytkownika jest już zajęta',
      unavailableClubName: 'Podana nazwa drużyny jest już zajęta',
      invalidCredentials: 'Nieprawidłowy login lub hasło'
   },
   success: {
      dataChangeSuccess: 'Dane zostały pomyślnie zmienione'
   },
   info: {
      requiredField: 'To pole nie może pozostać puste',
      emailTip: 'Podaj prawidłowy adres email (np. jan.kowalski@gmail.com)',
      usernameTip: 'Nazwa użytkownika musi zawierać 3-20 znaków (liter oraz cyfr)',
      passwordTip: 'Hasło musi zawierać 8-25 znaków (liter oraz cyfr)',
      equalsPasswords: 'Hasła muszą być identyczne',
      clubNameTip: 'Nazwa drużyny musi zawierać 3-20 znaków (liter oraz cyfr)',
      arenaNameTip: 'Pole musi zawierać 5-27 znaków (litery oraz cyfry)',
      speechTip: 'Wypowiedź zawodnika powinna zawierać maksymalnie 150 znaków (liter, cyfr oraz znaków interpunkcyjnych)'
   }
}