const { randomizer } = require('../helper-functions');
const Fieldplayer = require('../player-skillset-models/Fieldplayer');
const Goalkeeper = require('../player-skillset-models/Goalkeeper');

const firstnamesList = ['John', 'Paul', 'Lukas', 'Thomas', 'Carol'];
const lastnamesList = ['Smith', 'Oakwood', 'Baker', 'Hammerhand', 'Whatever'];
const footList = ['Both', 'Left', 'Right'];
const positionsList = [
   { position: 'GK',
   skillsFactors: {
      RE: 1,
      GP: 1,
      IN: 0.7,
      CT: 0.7,
      OR: 0.7,
   }},
   { position: 'RWB',
   skillsFactors: {
      SC: 0.5,
      OP: 0.7,
      BC: 0.5,
      PA: 0.7,
      AE: 0.5,
      CO: 0.8,
      TA: 0.7,
      DP: 0.7
   }},
   { position: 'RB',
   skillsFactors: {
      SC: 0.1,
      OP: 0.1,
      BC: 0.5,
      PA: 0.7,
      AE: 0.3,
      CO: 0.6,
      TA: 0.9,
      DP: 0.7
   }},
   { position: 'RM',
   skillsFactors: {
      SC: 0.5,
      OP: 0.4,
      BC: 0.7,
      PA: 1,
      AE: 0.2,
      CO: 0.6,
      TA: 0.7,
      DP: 0.5
   }},
   { position: 'RW',
   skillsFactors: {
      SC: 0.5,
      OP: 0.9,
      BC: 0.6,
      PA: 0.9,
      AE: 0.2,
      CO: 0.5,
      TA: 0.4,
      DP: 0.4
   }},
   { position: 'CB',
   skillsFactors: {
      SC: 0.1,
      OP: 0.1,
      BC: 0.1,
      PA: 0.3,
      AE: 0.8,
      CO: 0.6,
      TA: 1,
      DP: 1
   }},	
   {position: 'CM', 
   skillsFactors: {
      SC: 0.5,
      OP: 0.4,
      BC: 0.7,
      PA: 1,
      AE: 0.2,
      CO: 0.6,
      TA: 0.7,
      DP: 0.5
   }},
   {position: 'DM', 
   skillsFactors: {
      SC: 0.4,
      OP: 0.2,
      BC: 0.7,
      PA: 1,
      AE: 0.2,
      CO: 0.6,
      TA: 0.8,
      DP: 0.7
   }}, 
   {position: 'OM', 
   skillsFactors: {
      SC: 0.5,
      OP: 0.8,
      BC: 0.9,
      PA: 0.8,
      AE: 0.2,
      CO: 0.5,
      TA: 0.3,
      DP: 0.3
   }},
   {position: 'FW', 
   skillsFactors: {
      SC: 0.8,
      OP: 0.8,
      BC: 0.8,
      PA: 0.4,
      AE: 0.8,
      CO: 0.6,
      TA: 0.3,
      DP: 0.3
   }},
   {position: 'SN', 
   skillsFactors: {
      SC: 1,
      OP: 1,
      BC: 1,
      PA: 0.3,
      AE: 0.5,
      CO: 0.5,
      TA: 0.1,
      DP: 0.1
   }},
   {position: 'LWB',
   skillsFactors: {
      SC: 0.5,
      OP: 0.7,
      BC: 0.5,
      PA: 0.7,
      AE: 0.5,
      CO: 0.8,
      TA: 0.7,
      DP: 0.7
   }},
   {position: 'LB',
   skillsFactors: {
      SC: 0.1,
      OP: 0.1,
      BC: 0.5,
      PA: 0.7,
      AE: 0.3,
      CO: 0.6,
      TA: 0.9,
      DP: 0.7
   }},
   {position: 'LM',
   skillsFactors: {
      SC: 0.5,
      OP: 0.4,
      BC: 0.7,
      PA: 1,
      AE: 0.2,
      CO: 0.6,
      TA: 0.7,
      DP: 0.5
   }},
   {position: 'LW',
   skillsFactors: {
      SC: 0.5,
      OP: 0.9,
      BC: 0.6,
      PA: 0.9,
      AE: 0.2,
      CO: 0.5,
      TA: 0.4,
      DP: 0.4
   }}];

class PlayerGenerator {
   constructor(config) {
      this.nationality = config.nationality;
      this.age = this.setAge(config.age);
      this.talentRate = +config.talentRate;
      this.firstName = this.setNames(firstnamesList);
      this.lastName = this.setNames(lastnamesList);
      this.preferedLeg = this.setPreferedLeg(footList);
      this.positionCredentials = this.setPosition(positionsList, this.preferedLeg);
      this.skills = this.setSkills(this.talentRate, this.positionCredentials, this.age);
      this.skillsRate = this.setSkillsRate(this.skills, this.positionCredentials.skillsFactors);
      this.salary = this.setSalary(this.skillsRate, this.talentRate, this.age, this.skills, this.positionCredentials.skillsFactors);
   }

   setAgeFactor(age) {
      let { years, months } = age;

      if(months >= 5)
         years += 0.5;

      if(years >= 44) return 0.1;
      else if(years >= 42 && years < 44) return 0.2;
      else if((years >= 15 && years < 18) || (years >= 40 && years < 42)) return 0.3;
      else if((years >= 18 && years < 20) || (years >= 38 && years < 40)) return 0.4;
      else if((years >= 20 && years < 22) || (years >= 36 && years < 38)) return 0.5;
      else if((years >= 22 && years < 24) || (years >= 35 && years < 36)) return 0.6;
      else if((years >= 24 && years < 26) || (years >= 33 && years < 35)) return 0.7;
      else if((years >= 26 && years < 27) || (years >= 32 && years < 33)) return 0.8;
      else if(years >= 27 && years < 32) return 0.9;
   }

   getFullAge() {
      return `${this.age.years}l, ${this.age.months}m, ${this.age.days}d`;
   }

   setNames(namesList) {
      return namesList[randomizer(0, namesList.length - 1)];
   }

   setAge(age) {
      let years;
      let months = randomizer(0, 11);
      let days = randomizer(0, 31);

      if(age === 'junior')
         years = randomizer(15, 21);
      else
         years = randomizer(22, 35);

      return { years, months, days };
   }

   setPreferedLeg(footList) {
      const value = randomizer(0, 10);

      if(value === 0)
         return footList[value];
      else if (value >= 7)
         return footList[1];
      else
         return footList[2];
   }

   setPosition(positionsList, preferedLeg) {
      const role = this.setRole();

      if(role === 'Goalkeeper')
         return positionsList[0];
      else {
         if(preferedLeg === 'Right')
            return positionsList[randomizer(1, 10)];
         else if(preferedLeg === 'Left')
            return positionsList[randomizer(6, positionsList.length - 1)];
         else
            return positionsList[randomizer(1, positionsList.length - 1)];
      }
   }

   setRole() {
      const value = randomizer(0, 10);

      if(value === 0)
         return 'Goalkeeper';
      else
         return 'Fieldplayer';
   }

   setSkills(talentRate, positionCredentials, age) {
      const ageFactor = this.setAgeFactor(age);

      if(positionCredentials.position === 'GK') {
         const skillSet = new Goalkeeper(talentRate, positionCredentials.skillsFactors, ageFactor);
         return skillSet.getSkills();
      } else {
         const skillSet = new Fieldplayer(talentRate, positionCredentials.skillsFactors, ageFactor);
         return skillSet.getSkills();
      }
   }

   calcSumSkillsValues(skills) {
      return Object.keys(skills).reduce((total, skill) => {
         return total + skills[skill].current;
      }, 0);
   }

   calcSumSkillsFactors(skillsFactors) {
      return Object.keys(skillsFactors).reduce((total, skill) => {
         return total + (skillsFactors[skill] * 100);
      }, 0);
   }

   setSkillsRate(skills, skillsFactors) {
      const sumSkillsValues = this.calcSumSkillsValues(skills);
      const sumSkillsFactors = this.calcSumSkillsFactors(skillsFactors);
      const percentageRate = (sumSkillsValues / sumSkillsFactors) * 100;

      if(percentageRate <= 19)
         return 1;
      else if(percentageRate > 19 && percentageRate <= 29)
         return 2;
      else if(percentageRate > 29 && percentageRate <= 39)
         return 3;
      else if(percentageRate > 39 && percentageRate <= 49)
         return 4;
      else if(percentageRate > 49 && percentageRate <= 59)
         return 5;
      else if(percentageRate > 59 && percentageRate <= 69)
         return 6;
      else if(percentageRate > 69 && percentageRate <= 79)
         return 7;
      else if(percentageRate > 79 && percentageRate <= 89)
         return 8;
      else if(percentageRate > 89)
         return 9;
   }

   setSalary(skillsRate, talentRate, age, skills, skillsFactors) {
      return (Math.round(skillsRate / talentRate) * age.years * this.calcSumSkillsValues(skills));
   }
}

module.exports = PlayerGenerator;