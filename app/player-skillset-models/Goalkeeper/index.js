const { skillRandomizer } = require('../../helper-functions');

class GoalkeeperSkillset {
   constructor(talentRate, skillsFactors, ageFactor) {
      this.talentRate = talentRate * 10;
      this.skillsFactors = skillsFactors;
      this.ageFactor = ageFactor;
      this.RE = this.calcSkill('RE');
      this.GP = this.calcSkill('GP');
      this.IN = this.calcSkill('IN');
      this.CT = this.calcSkill('CT');
      this.OR = this.calcSkill('OR');
   }

   calcSkill(skill) {
      const potential = +this.calcPotentialSkillValue(skill);
      const current = +this.calcCurrentSkillValue(potential);

      return { current, potential };
   }

   calcPotentialSkillValue(skill) {
      const baseValue = this.skillsFactors[skill] * this.talentRate;
      return skillRandomizer(baseValue);
   }
   calcCurrentSkillValue(potential) {
      const baseValue = potential * this.ageFactor;
      const currentSkillvalue = skillRandomizer(baseValue);
      
      if(currentSkillvalue > potential)
         return potential;
      else
         return currentSkillvalue;
   }

   getSkills() {
      return { 
         RE: this.RE, 
         GP: this.GP, 
         IN: this.IN, 
         CT: this.CT, 
         OR: this.OR
      };
   }
}

module.exports = GoalkeeperSkillset;