const { skillRandomizer } = require('../../helper-functions');

class FieldplayerSkillset {
   constructor(talentRate, skillsFactors, ageFactor) {
      this.talentRate = talentRate * 10;
      this.skillsFactors = skillsFactors;
      this.ageFactor = ageFactor;
      this.SC = this.calcSkill('SC');
      this.OP = this.calcSkill('OP');
      this.BC = this.calcSkill('BC');
      this.PA = this.calcSkill('PA');
      this.AE = this.calcSkill('AE');
      this.CO = this.calcSkill('CO');
      this.TA = this.calcSkill('TA');
      this.DP = this.calcSkill('DP');
   }

   calcSkill(skill) {
      const potential = +this.calcPotentialSkillValue(skill);
      const current = +this.calcCurrentSkillValue(potential);

      return { current, potential };
   }

   calcPotentialSkillValue(skill) {
      const baseValue = this.skillsFactors[skill] * this.talentRate;
      return skillRandomizer(baseValue);
   }
   calcCurrentSkillValue(potential) {
      const baseValue = potential * this.ageFactor;
      const currentSkillvalue = skillRandomizer(baseValue);
      
      if(currentSkillvalue > potential)
         return potential;
      else
         return currentSkillvalue;
   }

   getSkills() {
      return { 
         SC: this.SC, 
         OP: this.OP, 
         BC: this.BC, 
         PA: this.PA, 
         AE: this.AE, 
         CO: this.CO, 
         TA: this.TA, 
         DP: this.DP 
      };
   }
}

module.exports = FieldplayerSkillset;