function randomizer(min = 15, max = 100, factor = 1) {
   return Math.floor((Math.random() * (max - min + 1)) + min) * factor;
}

function skillRandomizer(baseValue) {
   let value = Math.floor(Math.random() * ((baseValue + 15) - baseValue + 1) + baseValue);
   
   if(value < 15)
      return 15;
   else if(value > 100)
      return 100;
   else
      return value;
}

module.exports = {
   randomizer,
   skillRandomizer
}