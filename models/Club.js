const mongoose = require('mongoose');
const { connection, Schema, autoIncrement } = require('../config/db');
const { PlayerSchema } = require('./Player');

const ClubSchema = new Schema({
   owner: {
      type: Schema.Types.ObjectId,
      ref: 'User'
   },
   clubName: {
      type: String,
      unique: true,
      required: true,
      trim: true,
      minlength: 3,
      maxlength: 20
   },
   arenaName: {
      type: String,
      unique: true,
      trim: true,
      minlength: 5,
      maxlength: 27
   },
   budget: {
      type: Number,
      default: 25000
   },
   teamPlayers: [PlayerSchema]
});

ClubSchema.plugin(autoIncrement.plugin, 'Club');

const Club = connection.model('Club', ClubSchema);

module.exports = Club;