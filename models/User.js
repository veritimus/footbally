const mongoose = require('mongoose');
const { connection, Schema } = require('../config/db');
const Club = require('./Club');
const bcrypt = require('bcrypt');
const { invalidCredentials } = require('../app/alerts').errors;

const UserSchema = new Schema({
   email: {
      type: String,
      lowercase: true,
      unique: true,
      required: true,
      trim: true
   },
   username: {
      type: String,
      unique: true,
      required: true,
      trim: true,
      minlength: 3,
      maxlength: 20
   },
   password: {
      type: String,
      required: true,
      minlength: 8
   },
   dateOfRegistration: {
      type: Date,
      default: Date.now
   },
   club: {
      type: Number,
      ref: 'Club'
   }
}, { runSettersOnQuery: true });

UserSchema.statics.register = (userData, cb) => {  
   bcrypt.hash(userData.password, 8, (err, hashedPassword) => {
      if(err)
         return next(err);

      User.create({ email: userData.email, username: userData.username, password: hashedPassword }, (err, user) => {
         if(err)
            return cb(err);
   
         Club.create({ owner: user._id, clubName: userData.clubName, arenaName: userData.clubName + '\'s Arena' }, (err, club) => {
            if(err)
               return cb(err);
            
            cb(null, user);
         });
      });
   }); 
}

UserSchema.statics.authenticate = (userData, cb) => {
   User.findOne({ email: userData.email }, (err, user) => {
      if(err) 
         return cb(err);
      else if (!user)
         return cb(invalidCredentials);

      bcrypt.compare(userData.password, user.password, (err, result) => {
         if(err)
            return cb(err);
         else if(result === true)
            return cb(null, user);
         else
            cb();
      });
   });
};

const User = connection.model('User', UserSchema);

module.exports = User;