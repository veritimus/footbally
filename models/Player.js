const mongoose = require('mongoose');
const { connection, Schema, autoIncrement } = require('../config/db');

const PlayerSchema = new Schema({
   firstName: {
      type: String,
      required: true,
      trim: true
   },
   lastName: {
      type: String,
      required: true,
      trim: true
   },
   position: {
      type: String,
      required: true
   },
   nationality: {
      type: String,
      required: true
   },
   age: {
      years: {
         type: Number,
         required: true,
         min: 15
      },
      months: {
         type: Number,
         required: true,
         min: 0,
         max: 11
      },
      days: {
         type: Number,
         required: true,
         min: 0,
         max: 31
      }
   },
   skillsRate: {
      type: Number,
      required: true,
      min: 1,
      max: 9
   },
   talentRate: {
      type: Number,
      required: true,
      min: 1,
      max: 9
   },
   preferedLeg: {
      type: String,
      required: true
   },
   salary: {
      type: Number,
      required: true,
      min: 0
   },
   skills: {
      RE: {
         current: {
            type: Number,
            min: 15,
            max: 100
         },
         potential: {
            type: Number,
            min: 15,
            max: 100
         }
      },
      GP: {
         current: {
            type: Number,
            min: 15,
            max: 100
         },
         potential: {
            type: Number,
            min: 15,
            max: 100
         }
      },
      IN: {
         current: {
            type: Number,
            min: 15,
            max: 100
         },
         potential: {
            type: Number,
            min: 15,
            max: 100
         }
      },
      CT: {
         current: {
            type: Number,
            min: 15,
            max: 100
         },
         potential: {
            type: Number,
            min: 15,
            max: 100
         }
      },
      OR: {
         current: {
            type: Number,
            min: 15,
            max: 100
         },
         potential: {
            type: Number,
            min: 15,
            max: 100
         }
      },
      SC: {
         current: {
            type: Number,
            min: 15,
            max: 100
         },
         potential: {
            type: Number,
            min: 15,
            max: 100
         }
      },
      OP: {
         current: {
            type: Number,
            min: 15,
            max: 100
         },
         potential: {
            type: Number,
            min: 15,
            max: 100
         }
      },
      BC: {
         current: {
            type: Number,
            min: 15,
            max: 100
         },
         potential: {
            type: Number,
            min: 15,
            max: 100
         }
      },
      PA: {
         current: {
            type: Number,
            min: 15,
            max: 100
         },
         potential: {
            type: Number,
            min: 15,
            max: 100
         }
      },
      AE: {
         current: {
            type: Number,
            min: 15,
            max: 100
         },
         potential: {
            type: Number,
            min: 15,
            max: 100
         }
      },
      CO: {
         current: {
            type: Number,
            min: 15,
            max: 100
         },
         potential: {
            type: Number,
            min: 15,
            max: 100
         }
      },
      TA: {
         current: {
            type: Number,
            min: 15,
            max: 100
         },
         potential: {
            type: Number,
            min: 15,
            max: 100
         }
      },
      DP: {
         current: {
            type: Number,
            min: 15,
            max: 100
         },
         potential: {
            type: Number,
            min: 15,
            max: 100
         }
      }
   },
   speech: {
      type: String,
      trim: true,
      default: 'Nothing'
   }
});

PlayerSchema.plugin(autoIncrement.plugin, 'Player');

const Player = connection.model('Player', PlayerSchema);

module.exports = { PlayerSchema, Player };