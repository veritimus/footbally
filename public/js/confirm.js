const btn = document.querySelector('#btn-delete');
const url = 'https://footbally.herokuapp.com' + btn.getAttribute('href');

btn.addEventListener('click', (event) => {
   event.preventDefault();
   const result = window.confirm('Czy jesteś pewien, że chcesz to zrobić? Decyzja jest nieodwracalna.');

   if(result) {
      window.location.replace(url);
   }
});