const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const dbConnection = require('./config/db').connection;
const MongoStore = require('connect-mongo')(session);
const router = require('./routes/router');
const favicon = require('serve-favicon');
const keys = require('./config/keys');

const app = express();

app.use(favicon(`${__dirname}/public/images/favicon.ico`));
app.use(session({
   secret: keys.sessionSecret,
   resave: true,
   saveUninitialized: false,
   store: new MongoStore({ mongooseConnection: dbConnection })
}));
app.use(express.static(`${__dirname}/public`));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use((req, res, next) => {
   if(req.session.userID || req.path === '/login' || req.path === '/register')
      return next();

   return res.redirect('/login');
});
app.use('/', router);

app.set('view engine', 'pug');

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
   console.log('Server is running on port ', PORT);
});